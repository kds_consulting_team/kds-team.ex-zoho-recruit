import logging
import sys
import requests
import csv
import os
from kbc.env_handler import KBCEnvHandler

# Environment setup
sys.tracebacklimit = 0


# Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)-8s : [line:%(lineno)3s] %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")


# logging.INFO("Packages imported.")
# configuration variables
KEY_API_TOKEN = '#api_token'
KEY_PERIOD_FROM = 'period_from'

MANDATORY_PARS = [KEY_API_TOKEN, KEY_PERIOD_FROM]
MANDATORY_IMAGE_PARS = []

APP_VERSION = '0.0.1'
OUT_FIELDS = ['Id', 'Updated_on', 'Content', 'Value']

# logging.INFO("Global variables set.")


class Component(KBCEnvHandler):

    def __init__(self):

        KBCEnvHandler.__init__(self, MANDATORY_PARS)

        try:

            self.validate_config()

        except ValueError as e:

            logging.error(e)
            sys.exit(1)

        self.token = self.cfg_params[KEY_API_TOKEN]
        self.dateFrom = self.cfg_params[KEY_PERIOD_FROM]
        self._output_table_path = os.path.join(
            self.tables_out_path, 'Candidates.csv')
        self._writer = csv.DictWriter(open(self._output_table_path, 'w+'),
                                      fieldnames=OUT_FIELDS,
                                      delimiter=',',
                                      quotechar='"',
                                      quoting=csv.QUOTE_ALL,
                                      extrasaction='ignore',
                                      restval='')
        self.url = 'https://recruit.zoho.com/recruit/private/json/Candidates/getRecords'

        self._writer.writeheader()

    def run(self):

        params = {'authtoken': self.token,
                  'scope': 'recruitapi',
                  'lastModifiedTime': self.dateFrom,
                  'newFormat': '2',
                  'fromIndex': '1',
                  'toIndex': '200'}
        while (1):
            logging.info('Fetching from index %s to index %s',
                         params['fromIndex'], params['toIndex'])
            print('Fetching from index %s to index %s' %
                  (params['fromIndex'], params['toIndex']))
            resp = requests.get(url=self.url, params=params)
            if resp.status_code != 200:
                logging.info('The response code is %s.', str(resp.status_code))
                sys.exit(1)
            try:
                if 'error' in resp.json()['response'].keys():
                    if resp.json()['response']['error']['code'] == '9422':
                        logging.info('No data to show.')
                        break
                    else:
                        logging.error('API request returned an error: %s. \
Please check https://www.zoho.com/recruit/api-new/error-messages.html for reference',
                                      resp.json()['response']['error'])
                        sys.exit(1)
            except KeyError:
                pass

            candidates = resp.json()['response']['result']['Candidates']['row']
            for i in candidates:
                vals = [j['val'] for j in i['FL']]
                ci_index = vals.index('CANDIDATEID')
                uo_index = vals.index('Updated On')
                CANDIDATE_ID = i['FL'][ci_index]['content']
                UPDATED_ON = i['FL'][uo_index]['content']
                for j in i['FL']:
                    self._writer.writerow({'Id': CANDIDATE_ID,
                                           'Updated_on': UPDATED_ON,
                                           'Content': j['content'],
                                           'Value': j['val']})

            params['fromIndex'] = str(int(params['fromIndex']) + 200)
            params['toIndex'] = str(int(params['toIndex']) + 200)

        self.configuration.write_table_manifest(self._output_table_path,
                                                incremental=True,
                                                primary_key=['Id', 'Updated_on', 'Content'])


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug = sys.argv[1]
    else:
        debug = True
    comp = Component()
    logging.info('Running the component.')
    comp.run()
    logging.info('Run succesful.')
    sys.exit(0)
