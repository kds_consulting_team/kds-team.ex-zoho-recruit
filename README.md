# Zoho Recruit Extractor

The Zoho Recruit extractor allows users to download information from the Candidates endpoint of the Zoho Recruit API. The extractor downloads information about candidates and saves them incrementally into the storage in NVP format.

## API limitations

The extractor itself is limited only by runtime, which is 1 hour. For the limitations of the API please refer to this page: https://www.zoho.com/recruit/api-new/api-limits.html

## Configuration

Configuration of the extractor is very simple, you need to input only the _API token_ and the _Period from date_ parameter. The process of getting an API token is described here: https://www.zoho.com/recruit/api-new/using-authentication-token.html#gen-auth. The Period from date denotes the minimum Updated On date by the records. You can either use relative reference such as `-1 day` or a full Timestamp such as `2019-06-01 22:00:00`. 


### Output

The output is a NVP table. The table is loaded incrementally into storage and uses `Id`, `Updated_on` and `Content` columns as primary keys. The table has one extra column - `Value`. The `Content` denotes the question, the `Value` holds the value. There are 56 fields at this point, so there are 56 rows for one candidate in the resulting table.



## Development

To run application locally or further develop it, run the following commands

```
docker-compose build
docker-compose run dev
```

which will build and run the `dev` image of application.
